package incubytes;

import java.util.Scanner;

public class LeapYear {
	public boolean isLeapYear(int year) {
		if (year % 400 == 0) {
			return true;
		}
		if (year % 100 == 0) {
			return false;
		}
		if (year % 4 == 0) {
			return true;
		}
		return false;
	}

	public String checkLeapYear() {
		String answer = "Not a leap year.";
		Scanner sc = new Scanner(System.in);
		int year = sc.nextInt();
		sc.close();
		if (year >= 1582) {
			if (isLeapYear(year)) {
				answer = "Year IS a Leap Year!";
			} else {
				answer = "Year IS NOT a Leap Year!";
			}
		} else {
			answer = "Enter year Value 1582 onwards";
		}
		return answer;
	}
}
