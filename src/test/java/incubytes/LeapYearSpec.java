package incubytes;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class LeapYearSpec {

	@Test
	public void isLeapYear() {
		LeapYear classUnderTest = new LeapYear();
		assertEquals(false, classUnderTest.isLeapYear(1));
		assertEquals(false, classUnderTest.isLeapYear(100));
		assertEquals(false, classUnderTest.isLeapYear(400));
		assertEquals(false, classUnderTest.isLeapYear(4));
		assertEquals(false, classUnderTest.isLeapYear(2213));
		assertEquals(true, classUnderTest.isLeapYear(2012));
		assertEquals(true, classUnderTest.isLeapYear(1600));
		assertEquals(true, classUnderTest.isLeapYear(2000));
		assertEquals(false, classUnderTest.isLeapYear(2100));
		assertEquals(false, classUnderTest.isLeapYear(2894));
	}
}
